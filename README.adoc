= Fortune in Go

This application is an implementation in Go of the Fortune Cookie application using the https://gin-gonic.com/[Gin] web framework.

== Requirements

You need the https://go.dev/[Go language]. To run the application:

[source,shell]
--
$ go run main.go
--

And connect to it in http://localhost:8080.

== Live Reload

To live reload the app during development, use another library called https://github.com/codegangsta/gin[gin]:

[source,shell]
--
$ go install github.com/codegangsta/gin@latest
$ gin --appPort 8080
--

Connect to the application on http://localhost:3000/, edit the Go file, and refresh your page.

== Container

You can use Docker or Podman to build the container image defined in the `Dockerfile`.
